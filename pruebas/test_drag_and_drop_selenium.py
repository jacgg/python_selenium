import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains


class TestSelectSelenium(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_select_selenium(self):
        driver = self.driver
        driver.get('https://jqueryui.com/droppable/')
        driver.switch_to.frame(0)
        source1 = driver.find_element_by_id('draggable')
        target1 = driver.find_element_by_id('droppable')
        actions2 = ActionChains(driver)
        actions2.drag_and_drop(source1, target1).perform()
        self.assertEqual(target1.text, "Dropped!")


if __name__ == '__main__':
    unittest.main()
