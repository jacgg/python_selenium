import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains


class TestSelectSelenium(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_screen_shot_all_window(self):
        driver = self.driver
        driver.get("https://www.saucedemo.com/")

        # screenshot current window
        driver.save_screenshot('./capturas/full_screenshot.png')
        time.sleep(5)

        # take screenshot to an element
        robot_image = driver.find_element_by_xpath(
            "//body/div[@id='root']/div[1]/div[2]/div[1]/div[2]")

        robot_image.screenshot('./capturas/element_screenshot_robot.png')


if __name__ == '__main__':
    unittest.main()
