import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TestSelectSelenium(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.quit()

    def test_espera_explicita(self):
        driver = self.driver
        driver.get("https://www.google.com.pe/")
        wait = WebDriverWait(driver, 10)
        element = wait.until(EC.element_to_be_clickable((By.XPATH, '//body/div[1]/div[3]/form[1]/div[1]/div[1]/div[3]/center[1]/input[1]')))
        # click the element
        element.click()


if __name__ == '__main__':
    unittest.main()
