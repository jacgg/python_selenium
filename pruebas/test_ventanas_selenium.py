import unittest
import time
from selenium import webdriver
from selenium.webdriver.support.ui import Select


class TestVentanasSelenium(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.quit()

    def test_select_selenium(self):
        driver = self.driver
        driver.get("https://robocorp.com/docs/quickstart-guide")

        create_account_link = driver.find_element_by_xpath("//a[contains(text(),'create an account')]")
        create_account_link.click()
        time.sleep(2)
        driver.switch_to.window(driver.window_handles[0])
        register_now = driver.find_element_by_xpath("//a[contains(text(),'Register now')]")
        register_now.click()
        time.sleep(2)

        driver.switch_to.window(driver.window_handles[2])
        registrar_button = driver.find_element_by_xpath(
            '//*[@id="webinar-form"]/article/div/div/form/div/div[2]/button/span')
        registrar_button.click()
        time.sleep(2)
        driver.switch_to.window(driver.window_handles[0])
        # driver.switch_to.default_content()
        time.sleep(2)

        # click en Get Started
        get_started_button = driver.find_element_by_xpath(
            "//body/div[@id='___gatsby']/div[@id='gatsby-focus-wrapper']/nav[1]/nav[1]/a[4]")

        get_started_button.click()
        time.sleep(5)
        # evento atras en la pestaña objectivo
        driver.back()
        driver.forward()
        time.sleep(5)

        driver.switch_to.window(driver.window_handles[2])
        time.sleep(5)
        driver.close()
        time.sleep(5)


if __name__ == '__main__':
    unittest.main()
