import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


class TestNavegabilidadSelenium(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    @unittest.skip("test")
    def test_interaccion_con_pagina_web(self):
        driver = self.driver
        driver.get("http://www.google.com")
        same_element_3 = driver.find_element_by_xpath(
            "//body/div[1]/div[3]/form[1]/div[1]/div[1]/div[1]/div[1]/div[2]/input[1]")
        same_element_3.send_keys("FIND ELEMETN BY XPATH")
        time.sleep(4)
        same_element_3.clear()
        same_element_4 = driver.find_element_by_css_selector(
            "div.L3eUgb:nth-child(2) div.o3j99.ikrT4e.om7nvf:nth-child(3) div.A8SBwf:nth-child(1) "
            "div.RNNXgb:nth-child(2) div.SDkEP div.a4bIc > input.gLFyf.gsfi:nth-child(3)")
        same_element_4.send_keys("FIND ELEMETN BY CSS SELECTOR")
        time.sleep(4)
        same_element_4.clear()

    def test_interaccion_enter(self):
        driver = self.driver
        driver.get("http://www.google.com")
        button_imagen = driver.find_element_by_xpath(
            "//a[contains(text(),'Imágenes')]")
        button_imagen.click()

        input_text = driver.find_element_by_xpath(
            "//body/div[@id='viewport']/div[@id='searchform']/div[@id='qbc']/form[@id='tsf']/div[1]/div[1]/div["
            "1]/div[1]/div[2]/input[1]")
        input_text.send_keys("armadillos", Keys.ENTER)
        time.sleep(4)


if __name__ == '__main__':
    unittest.main()


