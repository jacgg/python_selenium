import unittest
import time
from selenium import webdriver
from selenium.webdriver.support.ui import Select


class TestSelectSelenium(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_select_selenium(self):
        driver = self.driver
        driver.get("https://material.angular.io/components/select/overview")

        select = Select(driver.find_element_by_xpath("//select[@id='mat-input-0']"))
        select.select_by_index(2)
        time.sleep(2)
        select.select_by_visible_text("Audi")
        time.sleep(2)
        select.select_by_value("saab")


if __name__ == '__main__':
    unittest.main()
