import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

CD = r"D:\canvia\coe_rpa\capacitaciones\selenium\Taller_Python_3_Selenium\sesiones\test\driver\chromedriver.exe"


class TestEjemplo1(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome(executable_path=CD)
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    # @unittest.skip("test")
    def test_ingresar_nombre(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html")
        close_inical_mesaje = driver.find_element_by_xpath('//*[@id="at-cv-lightbox-close"]')
        close_inical_mesaje.click()
        time.sleep(2)

        # textbox input
        box_input = driver.find_element_by_id("user-message")
        box_input.send_keys("jose")

        # click button Show Message
        button_show_message = driver.find_element_by_xpath("//*[@id='get-input']/button")
        button_show_message.click()

        # get text from message
        span_display = driver.find_element_by_xpath('//*[@id="display"]')
        self.assertEqual(span_display.text, "jose")

    def test_check_all_checkbutton(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-checkbox-demo.html")

        time.sleep(2)
        options_list = [
            "//label[text()='Option 1']",
            "//label[text()='Option 2']",
            "//label[text()='Option 3']",
            "//label[text()='Option 4']",
        ]

        for s in options_list:
            check_button = driver.find_element_by_xpath(s)
            check_button.click()
        # self.driver.find_element(By.CSS_SELECTOR, ".checkbox:nth-child(3) .cb1-element").click()
        # self.driver.find_element(By.CSS_SELECTOR, ".checkbox:nth-child(4) .cb1-element").click()
        # self.driver.find_element(By.CSS_SELECTOR, ".checkbox:nth-child(5) .cb1-element").click()
        # self.driver.find_element(By.CSS_SELECTOR, ".checkbox:nth-child(6) .cb1-element").click()
        time.sleep(5)
        check_all_button = driver.find_element_by_id('check1')
        button_text = check_all_button.get_attribute('value')
        self.assertEqual(button_text, "Uncheck All")


if __name__ == '__main__':
    unittest.main()


