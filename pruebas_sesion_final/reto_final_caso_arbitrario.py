import sys
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.common.by import By


class TestRetoFinalCaso(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    @unittest.skip("temp")
    def test_reto_final_caso_arbitrario1(self):
        driver = self.driver
        driver.get("https://demoqa.com/text-box")
        fullname = driver.find_element_by_xpath(
            "//input[@id='userName']"
        )
        email = driver.find_element_by_xpath(
            "//input[@id='userEmail']"
        )
        current_address= driver.find_element_by_xpath(
            "//textarea[@id='currentAddress']"
        )
        permanent_address = driver.find_element_by_xpath(
            "//textarea[@id='permanentAddress']"
        )
        submit_button = driver.find_element_by_xpath(
            "//button[@id='submit']"
        )

        fullname.send_keys("Jose G.")
        email.send_keys("jguerrag@canvia.com")
        current_address.send_keys("Lima")
        permanent_address.send_keys("Lima")
        submit_button.click()

        time.sleep(5)

    @unittest.skip("temp")
    def test_reto_final_caso_arbitrario2(self):
        driver = self.driver
        driver.get("http://www.rpachallenge.com/")

        # codigo here!
    @unittest.skip("temp")
    def test_reto_final_caso_arbitrario3(self):
        driver = self.driver
        driver.get("http://www.rpachallenge.com/")
        # codigo here!

    @unittest.skip("temp")
    def test_reto_final_caso_arbitrario4(self):
        driver = self.driver
        driver.get("http://www.rpachallenge.com/")
        # codigo here!

    @unittest.skip("temp")
    def test_reto_final_caso_arbitrario5(self):
        driver = self.driver
        driver.get("http://www.rpachallenge.com/")

        # codigo here!

    def test_child(self):
        for i in [1, 2, 3]:
            with self.subTest(i=i):
                self.assertEqual(i*i, i**2)


if __name__ == '__main__':
    unittest.main()
