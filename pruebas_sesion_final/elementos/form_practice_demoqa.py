class PracticeFormDmoQa:
    def __init__(self):
        self.url = "https://demoqa.com/automation-practice-form"

    @staticmethod
    def first_name_component():
        first_name_component = {
            'id': 'firstName',
            "xpath": "//input[@id='firstName']"
        }
        return first_name_component

    @staticmethod
    def lastname_componente():
        lastname_componente = {
            'id': 'lastName',
            'xpath': "//input[@id='lastName']"
        }
        return lastname_componente

    @staticmethod
    def email_component():
        email_component = {
            'id': "userEmail",
            "xpath": "//input[@id='userEmail']"
        }
        return email_component

    @staticmethod
    def gender_radio_button_component():
        gender_radio_button_component = {
            'male': "//label[contains(text(),'Male')]",
            'female': "//label[contains(text(),'Female')]",
            "other": "//label[contains(text(),'Other')]"
        }
        return gender_radio_button_component

    @staticmethod
    def mobile_component():
        mobile_component = {
            'xpath': "//input[@id='userNumber']",
            "id": "userNumber"
        }
        return mobile_component

    @staticmethod
    def date_component():
        date_component = {
            'id': "dateOfBirthInput",
            "xpath": "//input[@id='dateOfBirthInput']"
        }
        return date_component

    @staticmethod
    def month_select_component():
        month_select_component = {
            'xpath': "//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/form[1]/div[5]/div[2]/div[2]/div["
                     "2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/select[1] "
        }
        return month_select_component

    @staticmethod
    def year_select_component():
        year_select_component = {
            "xpath": "//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/form[1]/div[5]/div[2]/div[2]/div["
                     "2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/select[1]"
        }
        return year_select_component

    @staticmethod
    def day_component(day=None, month=None):
        assert day is not None, "Es necesario indicar day"
        assert month is not None, "Es necesario indicar Mes"
        day_component = {
            'xpath': "//div[contains(text(),'{day}')]".format(day=day),
            "xpath_alter": "//div[text()={day} and contains(@aria-label,'{month}')]".format(day=day, month=month)
        }
        return day_component

    @staticmethod
    def subject():
        subject = {
            "id": "subjectsContainer",
            "xpath":
                "//input[@id='subjectsInput']"
        }
        return subject

    @staticmethod
    def hobbies_check_button():
        hobbies_check_button = {
            "sports": "//label[contains(text(),'Sports')]",
            "reading": "//label[contains(text(),'Reading')]",
            "music": "//label[contains(text(),'Music')]"
        }
        return hobbies_check_button

    @staticmethod
    def currect_address_input():
        currect_address_input = {
            "id": "currentAddress",
            "xpath": "//textarea[@id='currentAddress']"
        }
        return currect_address_input

    @staticmethod
    def submit_button():
        submit_button = {
            "id": "submit",
            "xpath": "//button[@id='submit']"
        }
        return submit_button

    @staticmethod
    def close_button():
        close_button = {
            "xpath": "//button[@id='closeLargeModal']",
            "id": "closeLargeModal"
        }
        return close_button

    @staticmethod
    def lista():
        lista = [
            {
                "firstname": "Juan",
                "lastname": "Rosas",
                "email": "juan@rosas.com",
                "gender": "male",
                "mobile": 9898989871,
                "date_birth": {
                    "day": "1",
                    "month": "January",
                    "anio": "1992"
                },
                "subject": "Maths",
                "hobbies": "sports"
            },
            {
                "firstname": "Diana",
                "lastname": "Espinoza",
                "email": "Diana@gmail.com",
                "gender": "female",
                "mobile": 9999999991,
                "date_birth": {
                    "day": "23",
                    "month": "August",
                    "anio": "1989"
                },
                "subject": "Arts",
                "hobbies": "reading"
            },
            {
                "firstname": "Jordi",
                "lastname": "kid",
                "email": "jordi@gmail.com",
                "gender": "other",
                "mobile": 6666666661,
                "date_birth": {
                    "day": "12",
                    "month": "November",
                    "anio": "1959"
                },
                "subject": "Economics",
                "hobbies": "music"
            }
        ]
        return lista
