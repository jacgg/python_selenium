import sys
import os
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.common.by import By
# from selectores import selector as static_selector
import pandas as pd
import numpy as np
# ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'files'))
# EXCEL_PATH = os.path.join(ROOT_DIR,"challenge.xlsx")

class TestReto1(unittest.TestCase):
    def setUp(self) -> None:
        # importamos el Excel -> dataframe
        # self.matriz_data = pd.read_excel(EXCEL_PATH)
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()



    def test_reto1(self):

        driver = self.driver
        driver.get("http://www.rpachallenge.com/")
        # codigo here!

        button_start = driver.find_element_by_xpath(
            "//button[contains(text(),'Start')]"
        )
        button_submit = driver.find_element_by_xpath(
            "//body/app-root[1]/div[2]/app-rpa1[1]/div[1]/div[2]/form[1]/input[1]"
        )
        role_in_company = driver.find_element_by_xpath(
            "//input[@ng-reflect-name='labelRole']"
        )

        first_name = driver.find_element_by_xpath(
            "//input[@ng-reflect-name='labelFirstName']"
        )
        phone_number = driver.find_element_by_xpath(
            "//input[@ng-reflect-name='labelPhone']"
        )
        company_name = driver.find_element_by_xpath(
            "//input[@ng-reflect-name='labelCompanyName']"
        )

        address = driver.find_element_by_xpath(
            "//input[@ng-reflect-name='labelAddress']"
        )
        last_name = driver.find_element_by_xpath(
            "//input[@ng-reflect-name='labelLastName']"
        )
        email = driver.find_element_by_xpath(
            "//input[@ng-reflect-name='labelEmail']"
        )
        button_start.click()
        email.send_keys("jose@gmail.com")
        button_submit.click()
        time.sleep(10)


        # for a, i in self.matriz_data.iterrows():
        #     for z in self.matriz_data[a]:
        #         email.clear()
        #         email.send_keys(z["Email"])
        #         time.sleep(10)


if __name__ == '__main__':
    unittest.main()
