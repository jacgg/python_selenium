import unittest
import time
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from pruebas_sesion_final.elementos.form_practice_demoqa import PracticeFormDmoQa
from selenium.webdriver.common.keys import Keys

class FormChildTest(unittest.TestCase):

    def setUp(self) -> None:
        # importamos el Excel -> dataframe
        # self.matriz_data = pd.read_excel(EXCEL_PATH)
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
        self.pfdqa = PracticeFormDmoQa()

        # selectores

    def tearDown(self) -> None:
        self.driver.close()

    def test_child_form_unitest(self):
        driver = self.driver
        pfdqa = self.pfdqa
        # open web en form test
        driver.get(pfdqa.url)
        for x in pfdqa.lista():
            with self.subTest("form sub test", i=x):
                # ingresar firstname
                firstname = driver.find_element_by_xpath(pfdqa.first_name_component().get("xpath"))
                firstname.clear()
                firstname.send_keys(x.get("firstname"))

                # Ingresar Last Name
                lastname = driver.find_element_by_xpath(pfdqa.lastname_componente().get("xpath"))
                lastname.clear()
                lastname.send_keys(x.get('lastname'))

                # Ingresar Email
                email = driver.find_element_by_xpath(pfdqa.email_component().get('xpath'))
                email.clear()
                email.send_keys(x.get('email'))

                # Select -> click Gender
                gender = driver.find_element_by_xpath(pfdqa.gender_radio_button_component().get(x.get("gender")))
                gender.click()

                # Ingresar Mobile
                mobile = driver.find_element_by_xpath(pfdqa.mobile_component().get('xpath'))
                mobile.clear()
                mobile.send_keys(x.get('mobile'))

                # Ingresar Date Of Birth
                date_birth = driver.find_element_by_xpath(pfdqa.date_component().get('xpath'))
                date_birth.click()
                #   Select Month
                birth_month = Select(driver.find_element_by_xpath(pfdqa.month_select_component().get("xpath")))
                birth_month.select_by_visible_text(x.get("date_birth").get("month"))

                #   Select year
                birth_year = Select(driver.find_element_by_xpath(pfdqa.year_select_component().get('xpath')))
                birth_year.select_by_value(x.get("date_birth").get('anio'))

                #   Select Day

                birth_day = driver.find_element_by_xpath(pfdqa.day_component(x.get("date_birth").get("day"),
                                                                             x.get("date_birth").get("month"))
                                                         .get('xpath_alter'))
                birth_day.click()
                time.sleep(2)

                # Ingresar Subjects
                subject = driver.find_element_by_xpath(pfdqa.subject().get("xpath"))
                subject.clear()
                subject.send_keys(x.get("subject"))
                time.sleep(2)
                subject.send_keys(Keys.TAB)

                # Click Hobbies
                hobbies = driver.find_element_by_xpath(pfdqa.hobbies_check_button().get(x.get("hobbies")))
                hobbies.click()
                time.sleep(2)

                # click Submit button
                submit_button = driver.find_element_by_xpath(pfdqa.submit_button().get('xpath'))
                submit_button.click()
                time.sleep(2)

                # click close button
                close_button = driver.find_element_by_xpath(pfdqa.close_button().get('xpath'))
                close_button.click()
                time.sleep(1)



















