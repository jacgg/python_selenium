# Unitest Python

## Anstalación
```bash
pip install -r .package
```
## Configuración
```bash
# Windows
venv\Script\activate
# Linux
source venv/bin/activate
```
## Ejecución
```bash
python test_runner_selenium.py
```