import os
import unittest
import HtmlTestRunner
from pruebas_sesion_final import test_reto_1, reto_final_caso_arbitrario, form_child_test
current_directory = os.getcwd()


class HtmlTestRunnerTestSuite(unittest.TestCase):

    def test_taller_python_selenium(self):

        consolidate_test = unittest.TestSuite()

        consolidate_test.addTests([
            unittest.defaultTestLoader.loadTestsFromTestCase(form_child_test.FormChildTest)
        ])
        output_file = open(
            r"{cd}\HTML_Test_Runner_ReportTest.html".format(cd=current_directory),
            "w"
        )

        html_runner = HtmlTestRunner.HTMLTestRunner(
            stream=output_file,
            report_title="HTML Reporting using HTML TEST RUNNER",
            descriptions="Reporting"
        )
        html_runner.run(consolidate_test)


if __name__ == '__main__':
    unittest.main()
